package cn.xsshome.body.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
/**
 * 视频转图片
 * @author 小帅丶
 */
public class VideoFrame2ImageUtil {
	private static FFmpegFrameGrabber fFmpegFrameGrabber;
	/**
	 * 根据需要更改flag的值
	 * 将视频文件帧处理并以“jpg”格式进行存储。
	 * 依赖FrameToBufferedImage方法：将frame转换为bufferedImage对象
	 * @param videoPath 视频本地路径地址包含视频文件名称和后缀名
	 * @param videoFileName 图片保存本地路径地址
	 */
	public static void grabberVideoFramer(String videoPath,String videoFramesPath) {
		//Frame对象
		Frame frame = null;
		//获取视频文件
		FFmpegFrameGrabber fFmpegFrameGrabber = new FFmpegFrameGrabber(videoPath);
		// 标识 如果获取每帧并保存为图片 请修改为: int flag = 0;
		int flag = fFmpegFrameGrabber.getLengthInFrames();
		try {
			fFmpegFrameGrabber.start();
			//获取视频每秒的帧数			
			double secondFramr = fFmpegFrameGrabber.getFrameRate();
			System.out.println("视频一秒的帧数: " + secondFramr);
			//获取视频总帧数			
			int ftp = fFmpegFrameGrabber.getLengthInFrames();
			System.out.println("视频时长(秒): " + ftp/fFmpegFrameGrabber.getFrameRate());
			System.out.println("开始运行视频提取帧，耗时较长");
			while (flag <= ftp) {
				// 文件绝对路径+名字
				String fileName = videoFramesPath + "/img_"+ String.valueOf(flag) + ".jpg";
				// 文件储存对象
				File outPut = new File(fileName);
				// 获取帧
				frame = fFmpegFrameGrabber.grabImage();
				if (frame != null) {
					ImageIO.write(FrameToBufferedImage(frame), "jpg", outPut);
				}
				flag++;
			}
			System.out.println("============运行结束============");
			fFmpegFrameGrabber.stop();
		} catch (IOException e) {
			System.out.println("出现异常了"+e.getMessage());
		}
	}
	/**
	 * 获取视频一帧内容转为BufferedImage
	 * 将视频文件帧处理并以“jpg”格式进行存储。
	 * 依赖FrameToBufferedImage方法：将frame转换为bufferedImage对象
	 * @param videoPath 视频本地路径地址包含视频文件名称和后缀名
	 */
	public static BufferedImage getVideoFramer4bufferedImage(String videoPath) {
		//Frame对象
		Frame frame = null;
		BufferedImage bufferedImage = null;
		//获取视频文件
		fFmpegFrameGrabber = new FFmpegFrameGrabber(videoPath);
		try {
			fFmpegFrameGrabber.start();
			//获取视频每秒的帧数			
			double secondFramr = fFmpegFrameGrabber.getFrameRate();
			System.out.println("视频一秒的帧数: " + secondFramr);
			//获取视频总帧数			
			int ftp = fFmpegFrameGrabber.getLengthInFrames();
			System.out.println("视频时长(秒): " + ftp/fFmpegFrameGrabber.getFrameRate());
			System.out.println("开始运行视频提取一帧并返回BufferedImage，耗时较短");
			// 获取帧
			frame = fFmpegFrameGrabber.grabImage();
			if (frame != null) {
				bufferedImage = FrameToBufferedImage(frame);
			}
			System.out.println("============运行结束============");
			fFmpegFrameGrabber.stop();
		} catch (IOException e) {
			System.out.println("出现异常了"+e.getMessage());
		}
		return bufferedImage;
	}
	/**
	 * FrameToBufferedImage Frame转BufferedImage
	 * @param frame
	 * @return BufferedImage
	 */
	public static BufferedImage FrameToBufferedImage(Frame frame) {
		// 创建BufferedImage对象
		Java2DFrameConverter converter = new Java2DFrameConverter();
		BufferedImage bufferedImage = converter.getBufferedImage(frame);
		return bufferedImage;
	}
}
